package json.parser;

public enum ElementType {
    BRACKET_LEFT,
    BRACKET_RIGHT,
    STRING,
    NUMBER,
    COMMA,
    COLON
}
