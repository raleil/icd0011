package json.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Formatter {
    public String format(Map<String, Object> data) {
        return object(data);
    }

    public String object(Map<String, Object> object) {
        StringBuilder sb = new StringBuilder();

        sb.append("{");
        sb.append(entries(object.entrySet()));
        sb.append("}");

        return sb.toString();
    }

    public String entries(Set<Map.Entry<String, Object>> entryList) {
        List<String> entries = new ArrayList<>();

        for (Map.Entry<String, Object> entry : entryList) {
            entries.add(this.entry(entry));
        }

        return String.join(",", entries);
    }

    public String entry(Map.Entry<String, Object> entry) {
        StringBuilder sb = new StringBuilder();

        sb.append(string(entry.getKey()));
        sb.append(":");
        if (entry.getValue() instanceof String) {
            sb.append(string((String) entry.getValue()));
        } else if (entry.getValue() instanceof Number) {
            sb.append(number((Number) entry.getValue()));
        } else if (entry.getValue() instanceof Map) {
            sb.append(object((Map<String, Object>) entry.getValue()));
        }

        return sb.toString();
    }

    public String string(String value) {
        return "\"" + value.toString() + "\"";
    }

    public String number(Number value) {
        return value.toString();
    }
}
