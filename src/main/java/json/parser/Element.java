package json.parser;

public class Element {

    private final ElementType type;
    private String value;

    public static final Element BRACKET_LEFT = new Element(ElementType.BRACKET_LEFT);
    public static final Element BRACKET_RIGHT = new Element(ElementType.BRACKET_RIGHT);
    public static final Element COMMA = new Element(ElementType.COMMA);
    public static final Element COLON = new Element(ElementType.COLON);

    public static Element string(String value) {
        return new Element(ElementType.STRING, value);
    }

    public static Element number(Integer value) {
        return new Element(ElementType.NUMBER, value.toString());
    }

    public Element(ElementType elementType) {
        this.type = elementType;
    }

    public Element(ElementType elementType, String value) {
        this.type = elementType;
        this.value = value;
    }

    public ElementType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        if (value != null) {
            return type.toString() + "(%s)".formatted(value);
        }

        return type.toString();
    }
}
