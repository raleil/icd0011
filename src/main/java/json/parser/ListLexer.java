package json.parser;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Character.*;

public class ListLexer {
    private final String data;
    private int pos = 0;

    public ListLexer(String data) {
        this.data = data;
    }

    public List<Element> getElements() {
        ArrayList<Element> elements = new ArrayList<>();

        while (!eof()) {
            if (isWhitespace(lookahead())) {
                consume();
            } else if ('{' == lookahead()) {
                elements.add(Element.BRACKET_LEFT);
                consume();
            } else if ('}' == lookahead()) {
                elements.add(Element.BRACKET_RIGHT);
                consume();
            } else if ('"' == lookahead()) {
                consume();
                elements.add(Element.string(readString()));
                consume();
            } else if (isLetter(lookahead())) {
                elements.add(Element.string(readString()));
            } else if (':' == lookahead()) {
                elements.add(Element.COLON);
                consume();
            } else if (isDigit(lookahead())) {
                elements.add(Element.number(readNumber()));
            } else if (',' == lookahead()) {
                elements.add(Element.COMMA);
                consume();
            }
        }

        return elements;
    }

    private Integer readNumber() {
        return Integer.parseInt(readString());
    }

    private String readString() {
        StringBuilder sb = new StringBuilder();

        while(!eof() && isLetterOrDigit(lookahead())) {
            sb.append(consume());
        }

        return sb.toString();
    }

    private boolean eof() {
        return pos == data.length();
    }

    private char consume() {
        return data.charAt(pos++);
    }

    private char lookahead() {
        return data.charAt(pos);
    }
}
