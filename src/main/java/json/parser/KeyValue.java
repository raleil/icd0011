package json.parser;

public class KeyValue<K, V> {
    private K key;
    private V value;

    public KeyValue(K key) {
        this.key = key;
    }

    public KeyValue(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public void setkey(K key) {
        this.key = key;
    }

    public K getKey() {
        return key;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public V getValue() {
        return value;
    }
}
