package json.parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Parser {
    private final List<Element> elements;
    private int pos = 0;

    public Parser(List<Element> elements) {
        this.elements = elements;
    }

    public Map<String, Object> parse() {
        return object();
    }

    private Map<String, Object> object() {
        match(ElementType.BRACKET_LEFT);
        Map<String, Object> kvs = keyValues();
        match(ElementType.BRACKET_RIGHT);

        return kvs;
    }

    private Map<String, Object> keyValues() {
        Map<String, Object> items = new HashMap<>();

        KeyValue<String, Object> kv = keyValue();
        items.put(kv.getKey(), kv.getValue());

        while (lookahead().getType() == ElementType.COMMA) {
            match(ElementType.COMMA);

            kv = keyValue();
            items.put(kv.getKey(), kv.getValue());
        }

        return items;
    }

    private KeyValue<String, Object> keyValue() {
        KeyValue<String, Object> kv = new KeyValue<>(string());

        match(ElementType.COLON);

        if (lookahead().getType() == ElementType.STRING) {
            String item = string();

            if (!item.equals("null")) {
                kv.setValue(item);
            }
        }else if (lookahead().getType() == ElementType.NUMBER) {
            kv.setValue(number());
        } else if (lookahead().getType() == ElementType.BRACKET_LEFT) {
            kv.setValue(object());
        } else {
            throw new Error("Expecting string, number or object; found " + lookahead());
        }

        return kv;
    }

    private String string() {
        return match(ElementType.STRING).getValue();
    }

    private Integer number() {
        return Integer.parseInt(match(ElementType.NUMBER).getValue());
    }

    private Element match(ElementType type) {
        if (lookahead().getType() == type) {
            return consume();
        } else {
            throw new RuntimeException(
                    "Unexpected token '%s' at position %s".formatted(lookahead(), pos));
        }
    }

    private Element lookahead() {
        return elements.get(pos);
    }

    private Element consume() {
        return elements.get(pos++);
    }
}
