package database.dao;

import database.model.Order;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDaoSlow {

    private DataSource dataSource;

    public OrderDaoSlow(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getAllOrders() {
        String sql = "SELECT * FROM \"order\"";

        try (Connection conn = dataSource.getConnection();
             Statement stmt = conn.createStatement()) {

            Thread.sleep(1000);

            ResultSet rs = stmt.executeQuery(sql);

            List<Order> orders = new ArrayList<>();
            while (rs.next()) {
                Order order = new Order(rs.getLong("id"),
                        rs.getString("orderNumber"),
                        null);

                orders.add(order);
            }

            return orders;

        } catch (SQLException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public Order getOrderById(Long orderId) {
        String sql = "SELECT * FROM \"order\" WHERE id = ?";

        try (Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)) {

            Thread.sleep(1000);

            ps.setLong(1, orderId);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return new Order(rs.getLong("id"),
                        rs.getString("orderNumber"),
                        null);
            }

            return null;

        } catch (SQLException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
