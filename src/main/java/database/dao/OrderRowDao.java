package database.dao;

import database.model.OrderRow;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderRowDao {

    private DataSource dataSource;

    public OrderRowDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public OrderRow insertOrderRow(OrderRow orderRow) {
        String sql = "INSERT INTO \"orderRow\" (order_id, itemName, quantity, price) values (?, ?, ?, ?)";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, new String[] {"id"})) {

            ps.setLong(1, orderRow.getOrderId());
            ps.setString(2, orderRow.getItemName());
            ps.setInt(3, orderRow.getQuantity());
            ps.setInt(4, orderRow.getPrice());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            if (!rs.next()) {
                throw new RuntimeException("Missing generated id!");
            }

            return new OrderRow(rs.getLong("id"),
                    orderRow.getOrderId(),
                    orderRow.getItemName(),
                    orderRow.getQuantity(),
                    orderRow.getPrice()
            );

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
