package database.dao;

import database.model.Order;
import database.model.OrderRow;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDao {

    private DataSource dataSource;

    public OrderDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Order insertOrder(Order order) {
        String sql = "INSERT INTO \"order\" (orderNumber) values (?)";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, new String[] {"id"})) {

            ps.setString(1, order.getOrderNumber());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            if (!rs.next()) {
                throw new RuntimeException("Missing generated id!");
            }

            return new Order(rs.getLong("id"),
                    order.getOrderNumber(),
                    order.getOrderRows());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Order> getAllOrders() {
        String sql = "SELECT * FROM \"order\" LEFT JOIN \"orderRow\" o on \"order\".id = o.order_id";

        try (Connection conn = dataSource.getConnection();
             Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                return parseOrders(rs);
            }

            return new ArrayList<>();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Order getOrderById(Long orderId) {
        String sql = "SELECT * FROM \"order\" LEFT JOIN \"orderRow\" o on \"order\".id = o.order_id WHERE \"order\".id = ?";

        try (Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, orderId);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return parseOrders(rs).get(0);
            }

            return null;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteOrderById(Long orderId) {
        String sql = "DELETE FROM \"order\" WHERE id = ?";

        try (Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, orderId);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Order> parseOrders(ResultSet rs) throws SQLException {
        List<Order> orders = new ArrayList<>();

        do {
            Order newOrder = parseOrder(rs);

            if (orders.isEmpty()) {
                orders.add(newOrder);
                continue;
            }

            Order currentOrder = null;

            for (Order order : orders) {
                if (order.getId().equals(newOrder.getId())) {
                    currentOrder = order;
                    break;
                }
            }

            if (currentOrder != null) {
                currentOrder.getOrderRows().addAll(newOrder.getOrderRows());
                continue;
            }

            orders.add(newOrder);
        } while(rs.next());

        return orders;
    }

    private static Order parseOrder(ResultSet rs) throws SQLException {
        Order order = new Order();

        order.setId(rs.getLong("id"));
        order.setOrderNumber(rs.getString("orderNumber"));
        order.setOrderRows(new ArrayList<>());
        order.getOrderRows().add(parseOrderRow(rs));

        return order;
    }

    private static OrderRow parseOrderRow(ResultSet rs) throws SQLException {
        OrderRow orderRow = new OrderRow();

        orderRow.setId(rs.getLong("id"));
        orderRow.setOrderId(rs.getLong("order_id"));
        orderRow.setItemName(rs.getString("itemName"));
        orderRow.setQuantity(rs.getInt("quantity"));
        orderRow.setPrice(rs.getInt("price"));

        return orderRow;
    }
}
