package database;

import org.apache.commons.dbcp2.BasicDataSource;
import util.ConnectionInfo;
import util.File;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {

    public static void createSchema(DataSource datasource) {
        try (Connection conn = datasource.getConnection();
             Statement stmt = conn.createStatement()) {

            String sql = File.readFileFromClasspath("schema.sql");
            stmt.executeUpdate(sql);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static DataSource createConnectionPool(ConnectionInfo connectionInfo) {
        BasicDataSource pool = new BasicDataSource();
        pool.setDriverClassName("org.postgresql.Driver");
        pool.setUrl(connectionInfo.getUrl());
        pool.setUsername(connectionInfo.getUser());
        pool.setPassword(connectionInfo.getPass());
        pool.setMaxTotal(2);
        pool.setInitialSize(1);

        try {
            pool.getLogWriter();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return pool;
    }
}
