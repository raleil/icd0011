package database.proxy;

import util.ConnectionInfo;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class ConnectionProvider implements DataSource {

    private ConnectionManager connectionManager;

    public ConnectionProvider(int poolSize, ConnectionInfo connectionInfo) {
        this.connectionManager = new ConnectionManager(connectionInfo, poolSize);
    }

    public Connection getConnection() throws SQLException {
        return (Connection) Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                new Class[] { Connection.class },
                new DynamicInvocationHandler(this.connectionManager)
        );
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return null;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    public int getInPool() {
        return connectionManager.getInPool();
    }

    public int getInUse() {
        return connectionManager.getInUse();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }

    public static class DynamicInvocationHandler implements InvocationHandler {

        private ConnectionManager connectionManager;
        private Connection connection;

        public DynamicInvocationHandler(ConnectionManager connectionManager) {
            this.connectionManager = connectionManager;
            this.connection = connectionManager.getConnection();
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("close")) {
                connectionManager.returnConnection(connection);
                return null;
            }

            return method.invoke(connection, args);
        }
    }
}
