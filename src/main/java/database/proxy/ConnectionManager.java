package database.proxy;

import util.ConnectionInfo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Stack;

public class ConnectionManager {

    private ConnectionInfo connectionInfo;
    private Stack<Connection> pool;
    private int inUse = 0;

    public ConnectionManager(ConnectionInfo connectionInfo, int size) {
        this.connectionInfo = connectionInfo;

        try {
            this.pool = createConnectionPool(size);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int getInPool() {
        return pool.size();
    }

    public int getInUse() {
        return inUse;
    }

    public Connection getConnection() {
        if (pool.isEmpty()) {
            try {
                Thread.sleep(1000);
                return getConnection();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        inUse++;
        return pool.pop();
    }

    public void returnConnection(Connection connection) {
        inUse--;
        pool.add(connection);
    }

    private Stack<Connection> createConnectionPool(int size) throws SQLException {
        Stack<Connection> pool = new Stack<>();

        for (int i = 0; i < size; i++) {
            pool.add(createConnection());
        }

        return pool;
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(
                this.connectionInfo.getUrl(),
                this.connectionInfo.getUser(),
                this.connectionInfo.getPass()
        );
    }
}
