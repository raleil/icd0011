package servlet;

import database.Database;
import database.dao.OrderDao;
import database.dao.OrderDaoSlow;
import database.dao.OrderRowDao;
import org.apache.commons.dbcp2.BasicDataSource;
import util.Config;
import util.ConnectionInfo;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.sql.SQLException;

@WebListener
public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        var context = sce.getServletContext();

        ConnectionInfo connectionInfo = Config.readConnectionInfo();

        DataSource dataSource = Database.createConnectionPool(connectionInfo);

        // hw05a DB connection pool
        //DataSource dataSource = new ConnectionProvider(2, connectionInfo);

        Database.createSchema(dataSource);

        context.setAttribute("orderDao", new OrderDao(dataSource));
        context.setAttribute("orderRowDao", new OrderRowDao(dataSource));
        context.setAttribute("orderDaoSlow", new OrderDaoSlow(dataSource));
        context.setAttribute("connectionProvider", dataSource);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        var context = sce.getServletContext();

        BasicDataSource dataSource = (BasicDataSource) context.getAttribute("dataSource");

        if (dataSource != null) {
            try {
                dataSource.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
