package servlet.orders;

import database.dao.OrderDao;
import database.model.Order;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet("/orders/form")
public class Form extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {
        response.setContentType("text/plain");
        var context = getServletContext();

        OrderDao orderDao = (OrderDao) context.getAttribute("orderDao");

        String body = request.getReader().lines().collect(Collectors.joining("\n"));
        String[] kv = body.split("=");

        Order order = new Order();
        order.setOrderNumber(kv[1]);

        order = orderDao.insertOrder(order);

        response.getWriter().print(order.getId());
    }

}
