package servlet.api;

import json.parser.Element;
import json.parser.Formatter;
import json.parser.ListLexer;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet("/api/parser")
public class Parser extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws IOException {
        response.setContentType("application/json");

        String body = request.getReader().lines().collect(Collectors.joining("\n"));

        List<Element> elements = new ListLexer(body).getElements();

        Map<String, Object> data = new json.parser.Parser(elements).parse();
        Map<String, Object> modifiedData = processData(data);

        String output = new Formatter().format(modifiedData);

        response.getWriter().print(output);
    }

    private Map<String, Object> processData(Map<String, Object> data) {
        Map<String, Object> modifiedData = new HashMap<>();

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            if (entry.getValue() instanceof String) {
                if (entry.getKey().length() > 0 && entry.getKey().length() <= 3) {
                    modifiedData.put(entry.getKey(), entry.getValue());
                } else if (entry.getKey().length() > 3) {
                    modifiedData.put(entry.getKey(), entry.getKey());
                }
            } else if (entry.getValue() instanceof Number) {
                modifiedData.put(entry.getKey(), Integer.parseInt(entry.getValue().toString()) * 2);
            } else if (entry.getValue() instanceof Map) {
                modifiedData.put(entry.getKey(), processData((Map<String, Object>) entry.getValue()));
            }
        }

        return modifiedData;
    }
}
