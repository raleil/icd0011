package servlet.api.orders;

import com.fasterxml.jackson.databind.ObjectMapper;
import database.dao.OrderDao;
import database.dao.OrderRowDao;
import database.model.Order;
import database.model.OrderRow;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/api/orders")
public class Orders extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                       HttpServletResponse response)
            throws IOException {
        response.setContentType("application/json");
        var context = getServletContext();

        OrderDao orderDao = (OrderDao) context.getAttribute("orderDao");

        String id = request.getParameter("id");

        if (id != null) {
            Order order = orderDao.getOrderById(Long.parseLong(id));

            response.getWriter().print(new ObjectMapper().writeValueAsString(order));
            return;
        }

        List<Order> orders = orderDao.getAllOrders();

        response.getWriter().print(new ObjectMapper().writeValueAsString(orders));
    }

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws IOException {
        response.setContentType("application/json");
        var context = getServletContext();

        OrderDao orderDao = (OrderDao) context.getAttribute("orderDao");
        OrderRowDao orderRowDao = (OrderRowDao) context.getAttribute("orderRowDao");

        String body = request.getReader().lines().collect(Collectors.joining("\n"));
        Order order = new ObjectMapper().readValue(body, Order.class);

        order = orderDao.insertOrder(order);

        if (order.getOrderRows() != null) {
            for (OrderRow orderRow : order.getOrderRows()) {
                orderRow.setOrderId(order.getId());
                orderRowDao.insertOrderRow(orderRow);
            }
        }

        response.getWriter().print(new ObjectMapper().writeValueAsString(order));
    }

    @Override
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response)
            throws IOException {
        response.setContentType("application/json");
        var context = getServletContext();

        OrderDao orderDao = (OrderDao) context.getAttribute("orderDao");

        String id = request.getParameter("id");

        if (id != null) {
            orderDao.deleteOrderById(Long.parseLong(id));
        }
    }
}
