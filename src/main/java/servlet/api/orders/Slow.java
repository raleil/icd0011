package servlet.api.orders;

import com.fasterxml.jackson.databind.ObjectMapper;
import database.dao.OrderDaoSlow;
import database.model.Order;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/api/orders/slow")
public class Slow extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {
        response.setContentType("application/json");
        var context = getServletContext();

        OrderDaoSlow orderDaoSlow = (OrderDaoSlow) context.getAttribute("orderDaoSlow");

        String id = request.getParameter("id");

        if (id != null) {
            Order order = orderDaoSlow.getOrderById(Long.parseLong(id));

            response.getWriter().print(new ObjectMapper().writeValueAsString(order));
        }

        List<Order> orders = orderDaoSlow.getAllOrders();

        response.getWriter().print(new ObjectMapper().writeValueAsString(orders));
    }
}
