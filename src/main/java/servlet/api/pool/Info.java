package servlet.api.pool;

import database.proxy.ConnectionProvider;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/pool/info")
public class Info extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {
        response.setContentType("application/json");
        var context = getServletContext();

        ConnectionProvider connectionProvider = (ConnectionProvider) context.getAttribute("connectionProvider");

        response.getWriter().print("{ \"inPool\": %s, \"inUse\": %s }".formatted(connectionProvider.getInPool(), connectionProvider.getInUse()));
    }
}
