
DROP TABLE IF EXISTS "orderRow";
DROP TABLE IF EXISTS "order";
DROP SEQUENCE IF EXISTS order_id;
DROP SEQUENCE IF EXISTS orderRow_id;

CREATE SEQUENCE order_id START WITH 1;

CREATE TABLE "order" (
                         id int  NOT NULL DEFAULT nextval('order_id'),
                         orderNumber varchar(255) NOT NULL,
                         CONSTRAINT order_pk PRIMARY KEY (id)
);


CREATE SEQUENCE orderRow_id START WITH 1;


CREATE TABLE "orderRow" (
                          id int  NOT NULL DEFAULT nextval('orderRow_id'),
                          order_id int REFERENCES "order" (id) ON DELETE CASCADE,
                          itemName varchar(128)  NOT NULL,
                          quantity int  NOT NULL,
                          price int  NOT NULL,
                          CONSTRAINT orderRow_pk PRIMARY KEY (id)
);